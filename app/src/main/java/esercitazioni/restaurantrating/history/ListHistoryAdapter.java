package esercitazioni.restaurantrating.history;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import esercitazioni.core.Restaurant;
import esercitazioni.restaurantrating.R;

public class ListHistoryAdapter extends RecyclerView.Adapter<ListHistoryAdapter.HistoryViewHolder> {

    private List<Restaurant> listRestaurants;

    ListHistoryAdapter() {
        this.listRestaurants = new ArrayList<>();
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        this.listRestaurants.clear();
        this.listRestaurants.addAll(restaurants);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_hisotry_layout, parent, false);
        return new HistoryViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final HistoryViewHolder holder, int position) {
        final Restaurant actualRestaurant = listRestaurants.get(position);

        holder.textRestaurantAddress.setText(actualRestaurant.getAddress());
        holder.textRatingValue.setText(Float.toString(actualRestaurant.getRating()));
        holder.textRestaurantName.setText(actualRestaurant.getName());
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);
        holder.textDate.setText(df.format(actualRestaurant.getDate()));

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.expandableLayout.isExpanded()) {
                    holder.expandableLayout.collapse();
                    holder.button.setImageDrawable(view.getResources().getDrawable(R.drawable.ic_expand_more));
                }
                else {
                    holder.expandableLayout.expand();
                    holder.button.setImageDrawable(view.getResources().getDrawable(R.drawable.ic_expand_less));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listRestaurants.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        private final TextView textRestaurantName;
        private final TextView textRestaurantAddress;
        private final TextView textRatingValue;
        private final ExpandableLinearLayout expandableLayout;
        private final ImageButton button;
        private final TextView textDate;

        private HistoryViewHolder(View v) {
            super(v);
            view = v;
            button = view.findViewById(R.id.expand);
            expandableLayout = view.findViewById(R.id.expandableLayout);
            textRestaurantName = view.findViewById(R.id.text_name_history);
            textRatingValue = view.findViewById(R.id.text_rating_history);
            textDate = view.findViewById(R.id.text_date_history);
            textRestaurantAddress = view.findViewById(R.id.text_address_history);
        }
    }
}
