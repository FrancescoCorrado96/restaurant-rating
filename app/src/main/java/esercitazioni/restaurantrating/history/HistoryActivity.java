package esercitazioni.restaurantrating.history;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import esercitazioni.core.Restaurant;
import esercitazioni.core.RestaurantRepository;
import esercitazioni.core.RestaurantViewModel;
import esercitazioni.restaurantrating.R;
import esercitazioni.restaurantrating.base.BaseActivity;


public class HistoryActivity extends BaseActivity implements RestaurantRepository.ListRestaurantLoader{

    @BindView(R.id.rcv_history)
    RecyclerView rcvHistory;

    ListHistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        RestaurantViewModel db = ViewModelProviders.of(this).get(RestaurantViewModel.class);
        db.setLoader(this);
        db.loadListRestaurants();

        buildDrawer(HistoryActivity.this);
        abToolbar.setTitle(R.string.history_activity_title);

        rcvHistory.setHasFixedSize(true);
        rcvHistory.setLayoutManager(new LinearLayoutManager(HistoryActivity.this));

        adapter = new ListHistoryAdapter();

        rcvHistory.setAdapter(adapter);
    }

    @Override
    public void onListRestaurantsLoaded(List<Restaurant> restaurants) {
        adapter.setRestaurants(restaurants);
    }
}
