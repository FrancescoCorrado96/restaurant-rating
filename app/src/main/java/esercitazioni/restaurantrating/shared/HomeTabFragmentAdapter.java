package esercitazioni.restaurantrating.shared;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import esercitazioni.core.Restaurant;
import esercitazioni.restaurantrating.map.MapViewFragment;
import esercitazioni.restaurantrating.restaurant.ListRestaurantFragment;


public class HomeTabFragmentAdapter extends FragmentPagerAdapter {
    private static final int NUM_PAGES = 2;

    public HomeTabFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new ListRestaurantFragment();
        } else if (position == 1){
           return new MapViewFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Lista ristoranti";
            case 1:
                return "Mappa ristoranti";
            default:
                return null;
        }
    }
}
