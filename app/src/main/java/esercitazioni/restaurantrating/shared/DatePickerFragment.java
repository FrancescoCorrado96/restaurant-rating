package esercitazioni.restaurantrating.shared;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import java.util.Calendar;
import esercitazioni.restaurantrating.R;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        EditText etDay = getActivity().findViewById(R.id.et_add_day);
        EditText etMonth = getActivity().findViewById(R.id.et_add_month);
        EditText etYear = getActivity().findViewById(R.id.et_add_year);
        final Calendar c = Calendar.getInstance();
        return new DatePickerDialog(getActivity(), DatePickerFragment.this,
                Integer.parseInt(etYear.getText().toString()),
                Integer.parseInt(etMonth.getText().toString()),
                Integer.parseInt(etDay.getText().toString()));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        EditText etDay = getActivity().findViewById(R.id.et_add_day);
        EditText etMonth = getActivity().findViewById(R.id.et_add_month);
        EditText etYear = getActivity().findViewById(R.id.et_add_year);

        etDay.setText(String.valueOf(dayOfMonth));
        etYear.setText(String.valueOf(year));
        etMonth.setText(String.valueOf(month));
    }
}
