package esercitazioni.restaurantrating.map;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import esercitazioni.core.Restaurant;
import esercitazioni.core.RestaurantRepository;
import esercitazioni.core.RestaurantViewModel;
import esercitazioni.restaurantrating.R;

public class MapViewFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, RestaurantRepository.ListRestaurantLoader {

    @BindView(R.id.llt_restaurant_details)
    LinearLayout lltRestaurantDetails;

    @BindView(R.id.image_restaurant_map)
    ImageView imageRestaurant;

    @BindView(R.id.txv_address_map)
    TextView txvAddress;

    @BindView(R.id.txv_name_map)
    TextView txvName;

    @BindView(R.id.btn_map)
    ImageButton btnMap;

    private MapView mapView;
    LatLngBounds.Builder builder;
    List<Restaurant> restaurants;
    GoogleMap gMap;
    RestaurantViewModel db;


    public MapViewFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);

        db = ViewModelProviders.of(this).get(RestaurantViewModel.class);
        restaurants = new ArrayList<>();
        db.setLoader(this);
        db.loadListRestaurants();

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle("MapViewBundleKey");
        }

        mapView = view.findViewById(R.id.mapview);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(MapViewFragment.this);

        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        gMap = googleMap;

        googleMap.setOnMarkerClickListener(MapViewFragment.this);

        builder = new LatLngBounds.Builder();

        populateMapWithMarkers();
    }

    private void populateMapWithMarkers() {
        if (gMap != null) {
            gMap.clear();
            for (Restaurant res : restaurants) {

                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());

                List<Address> adr = null;
                try {
                    adr = geocoder.getFromLocationName(res.getAddress(), 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (adr == null)
                    return;

                LatLng actualRes = new LatLng(adr.get(0).getLatitude(), adr.get(0).getLongitude());
                builder.include(actualRes);
                Marker marker = gMap.addMarker(new MarkerOptions()
                        .position(actualRes)
                        .title(res.getName()));
                marker.setTag(res);
            }
            if (!restaurants.isEmpty())
                gMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 200));
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Restaurant actualRestaurant = (Restaurant) marker.getTag();
        toggleRestaurantDetails(actualRestaurant);
        return false;
    }

    private void toggleRestaurantDetails(final Restaurant restaurant) {

        Animation slide = null;

        if (lltRestaurantDetails.getVisibility() == View.GONE) {
            slide = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
            txvAddress.setText(restaurant.getAddress());
            txvName.setText(restaurant.getName());

            if (!restaurant.getImages().isEmpty())
                imageRestaurant.setImageURI(Uri.parse(restaurant.getImage(0)));
            else
                imageRestaurant.setImageDrawable(getResources().getDrawable(R.mipmap.placeholder));

            btnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/?q=" + restaurant.getAddress()));
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            });

            lltRestaurantDetails.setVisibility(View.VISIBLE);
        } else {
            slide = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
            lltRestaurantDetails.setVisibility(View.GONE);
        }
        lltRestaurantDetails.startAnimation(slide);
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle("MapViewBundleKey");
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle("MapViewBundleKey", mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onResume() {
        db.loadListRestaurants();
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public void onListRestaurantsLoaded(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
        populateMapWithMarkers();
    }
}
