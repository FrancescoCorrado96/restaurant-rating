package esercitazioni.restaurantrating.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import esercitazioni.restaurantrating.R;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseInstanceIDS";
    public static final int NOTIFICATION_ID = 1;
    public static final String ACTION = "esercitazioni.restaurantrating.notifications";
    public static final String TYPE = "type";
    private static final String TYPE_SPLASH = "splash";
    public static final int PENDING_ID = 451;


    public MyFirebaseMessagingService() {
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d(TAG, "Refreshed token: " + s);
    }



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getNotification() != null) {
            prepareNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
        }
    }


    private void prepareNotification(String message, String title) {

        Notification.Builder builder = new Notification.Builder(this);
        builder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.drawable.ic_home)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setStyle(new Notification.BigTextStyle()
                        .setBigContentTitle(getString(R.string.app_name))
                        .setSummaryText(message)
                        .bigText(message));
        builder.setContentIntent(getPendingIntentSplash());



        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        }
    }

    private PendingIntent getPendingIntentSplash() {
        return PendingIntent.getBroadcast(this, PENDING_ID, getIntentSplash(),
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static Intent getIntentSplash() {
        return new Intent(ACTION)
                .putExtra(TYPE, TYPE_SPLASH);

    }
}

