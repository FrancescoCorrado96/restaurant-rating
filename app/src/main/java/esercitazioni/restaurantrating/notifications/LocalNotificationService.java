package esercitazioni.restaurantrating.notifications;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;

import esercitazioni.restaurantrating.R;

public class LocalNotificationService extends IntentService {

    private static final String TAG = "MyFirebaseInstanceIDS";
    public static final int NOTIFICATION_ID = 1;
    public static final String ACTION = "esercitazioni.restaurantrating.notifications";
    public static final String TYPE = "type";
    private static final String TYPE_SPLASH = "splash";
    public static final int PENDING_ID = 452;

    Thread notificationThread;

    public LocalNotificationService() {
        super("Hello");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        notificationThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    stopSelf();
                    return;
                }
                prepareNotification("Non perderti i ristoranti che hai visitato!!");
            }
        });
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        notificationThread.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        notificationThread.interrupt();
    }

    private void prepareNotification(String message) {

        Notification.Builder builder = new Notification.Builder(this);

        builder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(Uri.parse(("android.resource://" + getPackageName() + "/" + R.raw.notification_sound_raw)))
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setStyle(new Notification.BigTextStyle()
                        .setBigContentTitle(getString(R.string.app_name))
                        .setSummaryText(message)
                        .bigText(message));
        builder.setContentIntent(getPendingIntentSplash());

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        }
    }

    private PendingIntent getPendingIntentSplash() {
        return PendingIntent.getBroadcast(this, PENDING_ID, getIntentSplash(),
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static Intent getIntentSplash() {
        return new Intent(ACTION)
                .putExtra(TYPE, TYPE_SPLASH);

    }

}
