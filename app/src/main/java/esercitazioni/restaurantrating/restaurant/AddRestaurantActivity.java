package esercitazioni.restaurantrating.restaurant;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import esercitazioni.core.Restaurant;
import esercitazioni.core.RestaurantRepository;
import esercitazioni.core.RestaurantViewModel;
import esercitazioni.restaurantrating.R;
import esercitazioni.restaurantrating.base.BaseActivity;
import esercitazioni.restaurantrating.image.ListImagesAdapter;
import esercitazioni.restaurantrating.shared.DatePickerFragment;

public class AddRestaurantActivity extends BaseActivity implements RestaurantRepository.ActualRestaurantLoader{

    private static final int RC_CAMERA_IMAGE = 10;
    private static final int RC_CAMERA_PERMISSION = 11;
    private static final int RC_GALLERY_IMAGE = 12;

    @BindView(R.id.et_add_address)
    EditText etAddAddress;
    @BindView(R.id.et_add_name)
    EditText etAddName;
    @BindView(R.id.add_rating)
    RatingBar npAddRating;
    @BindView(R.id.et_add_day)
    EditText etAddDay;
    @BindView(R.id.et_add_month)
    EditText etAddMonth;
    @BindView(R.id.et_add_year)
    EditText etAddYear;
    @BindView(R.id.btn_add_restaurant)
    Button btnAddRestaurant;
    @BindView(R.id.rl_chose_input_image)
    RelativeLayout rlChosePhotoGallery;
    @BindView(R.id.rcv_images_added)
    RecyclerView rcvImagesAdded;
    @BindView(R.id.pgb_add_restaurant)
    ProgressBar progressBar;
    @BindView(R.id.layout_add_restaurant)
    CoordinatorLayout mainLayout;

    private Restaurant restaurant;
    private List<String> listImages;
    private RestaurantViewModel db;
    private boolean modifyFlag;
    ListImagesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_restaurant);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        listImages = new ArrayList<>();
        modifyFlag = false;


        mAdapter = new ListImagesAdapter();

        db = ViewModelProviders.of(this).get(RestaurantViewModel.class);
        db.setLoader(this);

        if (intent.getExtras() != null && intent.hasExtra(RESTAURANT_KEY)) {
            db.loadRestaurant(intent.getExtras().getInt(RESTAURANT_KEY));

            modifyFlag = true;
            buildToolbarWithBackButton(AddRestaurantActivity.this, getString(R.string.modify_restaurant_title));

        } else {
            progressBar.setVisibility(View.GONE);
            mainLayout.setVisibility(View.VISIBLE);
            Calendar cal = Calendar.getInstance();
            buildToolbarWithBackButton(AddRestaurantActivity.this, getString(R.string.add_restaurant_title));
            etAddYear.setText(String.valueOf(cal.get(Calendar.YEAR)));
            etAddMonth.setText(String.valueOf(cal.get(Calendar.MONTH)));
            etAddDay.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
            mainLayout.setVisibility(View.VISIBLE);
        }

        rcvImagesAdded.setHasFixedSize(true);
        rcvImagesAdded.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rcvImagesAdded.setAdapter(mAdapter);
    }

    @OnClick(R.id.btn_add_restaurant)
    public void onAddRestaurantClick() {

        String name = etAddName.getText().toString();
        String address = etAddAddress.getText().toString();
        float rating = npAddRating.getRating();
        Calendar cal = Calendar.getInstance();

        cal.set(Integer.valueOf(etAddYear.getText().toString()),
                Integer.valueOf(etAddMonth.getText().toString()),
                Integer.valueOf(etAddDay.getText().toString()));


        if(name.isEmpty() || address.isEmpty()) {
            Toast.makeText(AddRestaurantActivity.this, "Riempi tutti i campi!", Toast.LENGTH_SHORT).show();
        } else {
            if(modifyFlag) {
                db.updateRestaurant(new Restaurant(restaurant.getId(), name, address, rating, cal.getTime(), listImages));
                Intent resultIntent = new Intent();
                resultIntent.putExtra(MODIFY_RESULT, true);
                setResult(RESULT_OK, resultIntent);
            } else {
                db.insertRestaurant(new Restaurant(name, address, rating, cal.getTime(), listImages));
            }
            finish();
        }
    }


    @OnClick(R.id.button_get_position)
    public void getPosition() {
        if (ActivityCompat.checkSelfPermission(AddRestaurantActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(AddRestaurantActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AddRestaurantActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_POSITION);
            return;
        }
        LocationServices.
                getFusedLocationProviderClient(this)
                .getLastLocation()
                .addOnSuccessListener(AddRestaurantActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            try {
                                Geocoder geocoder = new Geocoder(AddRestaurantActivity.this, Locale.getDefault());
                                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                etAddAddress.setText(addresses.get(0).getAddressLine(0));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    boolean isOnImageChoosing = false;

    @OnClick(R.id.image_btn_choose_image)
    public void getImage()  {
        rlChosePhotoGallery.setVisibility(View.VISIBLE);
        //rlChosePhotoGallery.startAnimation();
        isOnImageChoosing = true;

    }

    Uri photoURI;

    @OnClick(R.id.imgbtn_photo_chose)
    public void onPhotoChose() {
        if (isOnImageChoosing) {
            if (ActivityCompat.checkSelfPermission(AddRestaurantActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = createImageFile();
                if (photoFile != null) {

                    photoURI = FileProvider.getUriForFile(this,
                            "esercitazioni.restaurantrating.provider",
                            photoFile);
                    pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(pictureIntent, RC_CAMERA_IMAGE);
                }

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, RC_CAMERA_PERMISSION);
            }
        }
    }

    private File createImageFile() {
        String imageFileName = "CACHE_IMG_";
        File img = null;
        try {
            img = File.createTempFile(imageFileName, ".jpg", getCacheDir());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }

    @OnClick(R.id.imgbtn_gallery_chose)
    public void onGalleryChose() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, RC_GALLERY_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_CAMERA_IMAGE && resultCode == RESULT_OK  ) {

            listImages.add(saveFileFromUri(photoURI));

        } else if (requestCode == RC_GALLERY_IMAGE && data != null && data.getData() != null) {

            listImages.add(saveFileFromUri(data.getData()));
        }

        rlChosePhotoGallery.setVisibility(View.GONE);
        //rlChosePhotoGallery.startAnimation();
        isOnImageChoosing = false;

        mAdapter.setImages(listImages);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RC_CAMERA_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onPhotoChose();
                }
                return;
            }
            case PERMISSION_REQUEST_POSITION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getPosition();
                }
                return;
            }
            case PICK_IMAGE_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getImage();
                }
                return;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void showDatePickerDialog(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "Date Picker");
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(AddRestaurantActivity.this)
                .setTitle(R.string.discard_add_restaurant_title)
                .setMessage(R.string.discard_add_restaurant_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!listImages.isEmpty()) {
                            for(String img : listImages) {
                                new File(img).delete();
                            }
                        }
                        AddRestaurantActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private String saveFileFromUri(Uri uri) {
        String extension = ".jpg";
        String fileName = "IMG_" + new Random().nextInt() + extension;
        String path = getFilesDir().getAbsolutePath();
        File outputFile = new File(path, fileName);
        while(outputFile.exists()) {
            outputFile = new File(path, "IMG_" + new Random().nextInt() + extension);
        }
        InputStream in = null;
        OutputStream out = null;
        try {
            in = getContentResolver().openInputStream(uri);
            if (in != null) {
                out = new FileOutputStream(outputFile);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return outputFile.getAbsolutePath();
    }

    @Override
    public void onRestaurantLoaded(Restaurant restaurant) {

        Calendar cal = Calendar.getInstance();

        this.restaurant = restaurant;
        cal.setTime(restaurant.getDate());

        etAddYear.setText(String.valueOf(cal.get(Calendar.YEAR)));
        etAddMonth.setText(String.valueOf(cal.get(Calendar.MONTH)));
        etAddDay.setText(String.valueOf(cal.get(Calendar.DATE)));

        etAddName.setText(restaurant.getName());
        etAddAddress.setText(restaurant.getAddress());

        npAddRating.setRating(restaurant.getRating());
        btnAddRestaurant.setText(R.string.modify);

        listImages.addAll(restaurant.getImages());
        mAdapter.setImages(listImages);

        progressBar.setVisibility(View.GONE);
        mainLayout.setVisibility(View.VISIBLE);
    }
}
