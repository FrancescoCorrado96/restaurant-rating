package esercitazioni.restaurantrating.restaurant;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import esercitazioni.core.Restaurant;
import esercitazioni.core.RestaurantRepository;
import esercitazioni.core.RestaurantViewModel;
import esercitazioni.restaurantrating.R;

import static esercitazioni.restaurantrating.base.BaseActivity.RESTAURANT_KEY;

public class ListRestaurantFragment extends Fragment implements ListRestaurantAdapter.OnRestaurantInteractionListener, RestaurantRepository.ListRestaurantLoader {

    @BindView(R.id.rcv_list_restaurant_home)
    RecyclerView rcvListRestaurantHome;

    ListRestaurantAdapter mAdapter;
    RestaurantViewModel db;

    public ListRestaurantFragment() {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_restaurant, container, false);
        ButterKnife.bind(this, view);

        rcvListRestaurantHome.setHasFixedSize(true);

        mAdapter = new ListRestaurantAdapter(this);

        db = ViewModelProviders.of(this).get(RestaurantViewModel.class);

        db.setLoader(this);

        db.loadListRestaurants();


        rcvListRestaurantHome.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvListRestaurantHome.setAdapter(mAdapter);

        return view;
    }

    @OnClick(R.id.fab_add_restaurant)
    public void addRestaurant() {
        if (getActivity() != null)
            getActivity().startActivity(new Intent(getActivity(), AddRestaurantActivity.class));
    }

    @Override
    public void onAddressClick(String address) {
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/?q=" + address));
        mapIntent.setPackage("com.google.android.apps.maps");
        if (getActivity() != null && mapIntent.resolveActivity(getActivity().getPackageManager()) != null)
            startActivity(mapIntent);

    }

    @Override
    public void onDetailClick(Restaurant restaurant) {
        Intent intent = new Intent(getActivity(), DetailRestaurantActivity.class);
        intent.putExtra(RESTAURANT_KEY, restaurant.getId());
        if (getActivity() != null)
            getActivity().startActivity(intent);
    }

    @Override
    public void onListRestaurantsLoaded(List<Restaurant> restaurants) {
        mAdapter.setRestaurants(restaurants);
    }

    @Override
    public void onResume() {
        super.onResume();
        db.loadListRestaurants();
    }
}
