package esercitazioni.restaurantrating.restaurant;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import esercitazioni.core.Restaurant;
import esercitazioni.restaurantrating.R;

public class ListRestaurantAdapter extends RecyclerView.Adapter<ListRestaurantAdapter.RestaurantViewHolder> {


    private List<Restaurant> listRestaurants;

    private final OnRestaurantInteractionListener listener;

    public interface OnRestaurantInteractionListener {
        void onAddressClick(String address);
        void onDetailClick(Restaurant restaurant);
    }

    ListRestaurantAdapter(OnRestaurantInteractionListener listener) {
        this.listener = listener;
        listRestaurants = new ArrayList<>();
    }

    public void setRestaurants(@NonNull List<Restaurant> restaurants) {
        this.listRestaurants.clear();
        this.listRestaurants.addAll(restaurants);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_restaurant_layout, parent, false);
        return new RestaurantViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
        final Restaurant actualRestaurant = listRestaurants.get(position);

        holder.textRestaurantAddress.setText(actualRestaurant.getAddress());
        holder.textRatingValue.setText(Float.toString(actualRestaurant.getRating()));
        holder.textRestaurantName.setText(actualRestaurant.getName());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onDetailClick(actualRestaurant);
            }
        });

        holder.imvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onAddressClick(actualRestaurant.getAddress());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listRestaurants.size();
    }

    public class RestaurantViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        private final TextView textRestaurantName;
        private final TextView textRestaurantAddress;
        private final ImageView imvLocation;
        private final TextView textRatingValue;

        private RestaurantViewHolder(View v) {
            super(v);
            view = v;
            textRestaurantName = view.findViewById(R.id.text_name_home);
            textRatingValue = view.findViewById(R.id.text_rating_value);
            imvLocation = view.findViewById(R.id.imv_location);
            textRestaurantAddress = view.findViewById(R.id.text_address_home);
        }
    }
}
