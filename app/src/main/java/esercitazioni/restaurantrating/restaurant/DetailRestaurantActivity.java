package esercitazioni.restaurantrating.restaurant;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import esercitazioni.core.Restaurant;
import esercitazioni.core.RestaurantRepository;
import esercitazioni.core.RestaurantViewModel;
import esercitazioni.restaurantrating.R;
import esercitazioni.restaurantrating.base.BaseActivity;
import esercitazioni.restaurantrating.image.ImagePagerAdapter;
import me.relex.circleindicator.CircleIndicator;

public class DetailRestaurantActivity extends BaseActivity implements RestaurantRepository.ActualRestaurantLoader{

    @BindView(R.id.text_address_detail)
    TextView textAddressDetail;
    @BindView(R.id.text_rating_detail)
    TextView textRatingDetail;
    @BindView(R.id.text_name_detail)
    TextView textNameDetail;
    @BindView(R.id.text_date_detail)
    TextView textDateDetail;
    @BindView(R.id.view_pager_detail)
    ViewPager viewPagerDetail;
    @BindView(R.id.image_indicator)
    CircleIndicator imageIndicator;
    @BindView(R.id.pgb_detail_restaurant)
    ProgressBar progressBar;
    @BindView(R.id.lly_detail_restaurant)
    LinearLayout body;

    private Restaurant actualRestaurant;
    private ImagePagerAdapter imageAdapter;
    private RestaurantViewModel db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_restaurant);
        buildToolbarWithBackButton(DetailRestaurantActivity.this, "Dettagli Ristorante");

        db = ViewModelProviders.of(this).get(RestaurantViewModel.class);
        db.setLoader(this);

        Intent intent = getIntent();
        if(intent.getExtras() != null && intent.hasExtra(RESTAURANT_KEY))
            db.loadRestaurant(intent.getExtras().getInt(RESTAURANT_KEY));
        else
            finish();
    }

    @OnClick(R.id.btn_open_map)
    public void openMap() {
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/?q=" + actualRestaurant.getAddress()));
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @OnClick(R.id.btn_modify)
    public void onModifyClick() {
        Intent newIntent = new Intent(DetailRestaurantActivity.this, AddRestaurantActivity.class);
        newIntent.putExtra(RESTAURANT_KEY, actualRestaurant.getId());
        startActivityForResult(newIntent, MODIFY_REQUEST_CODE);
    }

    @OnClick(R.id.btn_delete)
    public void onDeleteClick() {
        new AlertDialog.Builder(DetailRestaurantActivity.this)
                .setTitle(R.string.delete_dialog_title)
                .setMessage(R.string.confirm_delete_message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.deleteRestaurant(actualRestaurant);
                        Toast.makeText(DetailRestaurantActivity.this, "Eliminazione eseguita con successo", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.discard), null)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MODIFY_REQUEST_CODE && resultCode == RESULT_OK && data.getExtras() != null) {
            if(data.getExtras().getBoolean(MODIFY_RESULT)) {
                db.loadRestaurant(actualRestaurant.getId());
                progressBar.setVisibility(View.VISIBLE);
                body.setVisibility(View.GONE);
            }
        }
    }

    private void populateDetailRestaurant() {

        textNameDetail.setText(actualRestaurant.getName());
        textAddressDetail.setText(actualRestaurant.getAddress());
        textRatingDetail.setText(String.valueOf(actualRestaurant.getRating()));

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);
        textDateDetail.setText(df.format(actualRestaurant.getDate()));
        imageAdapter.setImages(actualRestaurant.getImages());

    }

    @Override
    public void onRestaurantLoaded(Restaurant restaurant) {
        actualRestaurant = restaurant;
        imageAdapter = new ImagePagerAdapter();

        populateDetailRestaurant();

        viewPagerDetail.setAdapter(imageAdapter);
        imageIndicator.setViewPager(viewPagerDetail);
        progressBar.setVisibility(View.GONE);
        body.setVisibility(View.VISIBLE);
    }
}
