package esercitazioni.restaurantrating.base;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import java.util.List;

import butterknife.BindView;
import esercitazioni.core.Restaurant;
import esercitazioni.core.RestaurantViewModel;
import esercitazioni.restaurantrating.R;
import esercitazioni.restaurantrating.history.ListHistoryAdapter;
import esercitazioni.restaurantrating.notifications.LocalNotificationService;
import esercitazioni.restaurantrating.shared.HomeTabFragmentAdapter;

public class MainActivity extends BaseActivity {

    @BindView(R.id.view_pager_home)
    ViewPager viewPagerHome;

    @BindView(R.id.tab_home)
    TabLayout tabHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buildDrawer(MainActivity.this);

        abToolbar.setTitle("Home");

        viewPagerHome.setAdapter(new HomeTabFragmentAdapter(getSupportFragmentManager()));
        tabHome.setupWithViewPager(viewPagerHome);

    }


}
