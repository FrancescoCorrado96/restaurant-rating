package esercitazioni.restaurantrating.base;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import esercitazioni.core.Restaurant;
import esercitazioni.core.RestaurantViewModel;
import esercitazioni.restaurantrating.BuildConfig;
import esercitazioni.restaurantrating.history.HistoryActivity;
import esercitazioni.restaurantrating.R;
import esercitazioni.restaurantrating.history.ListHistoryAdapter;
import esercitazioni.restaurantrating.notifications.LocalNotificationService;

public class BaseActivity extends AppCompatActivity {

    public static final int PERMISSION_REQUEST_POSITION = 1;
    public static final int PICK_IMAGE_REQUEST = 2;

    public static final String RESTAURANT_KEY = "restaurant_key";

    public static final String MODIFY_RESULT = "modify_result";

    public static final int MODIFY_REQUEST_CODE = 3;


    @BindView(R.id.ab_toolbar)
    public Toolbar abToolbar;

    public void buildToolbarWithBackButton(final Activity actualActivity, String activityName) {
        ButterKnife.bind(actualActivity);
        setSupportActionBar(abToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void buildDrawer(final Activity actualActivity) {
        ButterKnife.bind(actualActivity);

        new DrawerBuilder()
                .withActivity(this)
                .withToolbar(abToolbar)
                .withSelectedItem(actualActivity.getClass() == MainActivity.class ? 1 : 2)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withName("Home")
                                .withIcon(getResources().getDrawable(R.drawable.ic_home))
                                .withIdentifier(1)
                                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                    @Override
                                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                        if (actualActivity.getClass() != MainActivity.class) {
                                            startActivity(new Intent(actualActivity, MainActivity.class));
                                            actualActivity.finish();
                                        }
                                        return true;
                                    }
                                }),
                        new PrimaryDrawerItem()
                                .withName("Storico")
                                .withIcon(getResources().getDrawable(R.drawable.ic_folder))
                                .withIdentifier(2)
                                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                    @Override
                                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                        if (actualActivity.getClass() != HistoryActivity.class) {
                                            startActivity(new Intent(actualActivity, HistoryActivity.class));
                                            actualActivity.finish();
                                        }
                                        return true;
                                    }
                                }),
                        new SectionDrawerItem()
                                .withDivider(true)
                                .withName(R.string.contact_label),
                        new SecondaryDrawerItem()
                                .withIcon(getResources().getDrawable(R.drawable.ic_mail))
                                .withName(getString(R.string.mail))
                                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                    @Override
                                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                                        intent.setData(Uri.fromParts(
                                                "mailto", getString(R.string.mail), null));
                                        if (intent.resolveActivity(getPackageManager()) != null) {
                                            startActivity(intent);
                                        }
                                        return true;
                                    }
                                })
                                .withSelectable(false),
                        new SecondaryDrawerItem()
                                .withIcon(getResources().getDrawable(R.drawable.ic_phone))
                                .withName(getString(R.string.phone))
                                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                    @Override
                                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                        Intent intent = new Intent(Intent.ACTION_DIAL);
                                        intent.setData(Uri.parse("tel:" + getString(R.string.phone)));
                                        if (intent.resolveActivity(getPackageManager()) != null) {
                                            startActivity(intent);
                                        }
                                        return true;
                                    }
                                })
                                .withSelectable(false),
                        new SectionDrawerItem()
                                .withName("Versione app: " + BuildConfig.VERSION_NAME)
                                .withDivider(true)
                )
                .withHeader(R.layout.drawer_header)
                .build();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    Intent localNotificationIntent = null;

    @Override
    protected void onResume() {
        super.onResume();
        if (localNotificationIntent == null)
            localNotificationIntent = new Intent(this, LocalNotificationService.class);
        stopService(localNotificationIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        startService(localNotificationIntent);
    }
}
