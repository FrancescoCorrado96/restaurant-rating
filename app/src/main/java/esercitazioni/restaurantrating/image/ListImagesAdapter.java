package esercitazioni.restaurantrating.image;

import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import esercitazioni.restaurantrating.R;

public class ListImagesAdapter extends RecyclerView.Adapter<ListImagesAdapter.ImageViewHolder> {

    private List<String> images;

    public ListImagesAdapter() {
        this.images = new ArrayList<>();
    }

    public void setImages(List<String> images) {
        this.images.clear();
        this.images.addAll(images);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_image_layout, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageViewHolder holder, int position) {
        String actualImg = images.get(position);
        Picasso.get().load(new File(actualImg)).into(holder.imvImageAdded);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        private ImageView imvImageAdded;

        private ImageViewHolder(View v) {
            super(v);
            view = v;
            imvImageAdded = view.findViewById(R.id.imv_image_added_element);
        }
    }
}
