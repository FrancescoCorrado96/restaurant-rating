package esercitazioni.restaurantrating.image;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Francesco Corrado
 * on 24/07/2018.
 * 2018 Laboratorio Informatico.
 */
public class ImagePagerAdapter extends PagerAdapter {

    private List<String> images;

    public ImagePagerAdapter() {
        this.images = new ArrayList<>();
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }



    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageRestaurant = new ImageView(container.getContext());

        imageRestaurant.setScaleType(ImageView.ScaleType.FIT_CENTER);

        Picasso.get().load(new File(images.get(position))).resize(1024, 768).into(imageRestaurant);

        container.addView(imageRestaurant, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return imageRestaurant;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
