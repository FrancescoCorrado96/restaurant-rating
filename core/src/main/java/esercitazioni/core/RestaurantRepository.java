package esercitazioni.core;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

public class RestaurantRepository {

    private RestaurantDao restaurantDao;
    private ListRestaurantLoader listLoader;
    private ActualRestaurantLoader actualRestaurantLoader;

    public interface ListRestaurantLoader {
        void onListRestaurantsLoaded(List<Restaurant> restaurants);
    }

    public interface ActualRestaurantLoader {
        void onRestaurantLoaded(Restaurant restaurant);
    }

    RestaurantRepository(Application application) {
        restaurantDao = RestaurantDatabase.getDatabase(application).restaurantDao();
    }

    public void setLoaders (ListRestaurantLoader listLoader, ActualRestaurantLoader actualRestaurantLoader) {
        this.listLoader = listLoader;
        this.actualRestaurantLoader = actualRestaurantLoader;
    }

    public void loadListRestaurant() {
        new loadAllRestaurantsAsyncTask(restaurantDao, listLoader).execute();
    }

    public void loadRestaurant(int id) {
        new loadRestaurantFromIdAsyncTask(restaurantDao, actualRestaurantLoader).execute(id);
    }

    public void insertRestaurant (Restaurant restaurant) {
        new insertAsyncTask(restaurantDao).execute(restaurant);
    }

    public void deleteRestaurant (Restaurant restaurant) {
        new deleteAsyncTask(restaurantDao).execute(restaurant);
    }

    public void updateRestaurant (Restaurant restaurant) {
        new updateAsyncTask(restaurantDao).execute(restaurant);
    }

    private static class insertAsyncTask extends AsyncTask<Restaurant, Void, Void> {

        private RestaurantDao mAsyncTaskDao;

        insertAsyncTask(RestaurantDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Restaurant... params) {
            mAsyncTaskDao.insertRestaurant(params[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Restaurant, Void, Void> {

        private RestaurantDao mAsyncTaskDao;

        deleteAsyncTask(RestaurantDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Restaurant... params) {
            mAsyncTaskDao.deleteRestaurant(params[0]);
            return null;
        }
    }

    private static class updateAsyncTask extends AsyncTask<Restaurant, Void, Void> {

        private RestaurantDao mAsyncTaskDao;

        updateAsyncTask(RestaurantDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Restaurant... params) {
            mAsyncTaskDao.updateRestaurant(params[0]);
            return null;
        }
    }

    private static class loadRestaurantFromIdAsyncTask extends AsyncTask<Integer, Void, Restaurant> {

        private RestaurantDao mAsyncTaskDao;
        private ActualRestaurantLoader loader;

        loadRestaurantFromIdAsyncTask(RestaurantDao dao, ActualRestaurantLoader loader) {
            mAsyncTaskDao = dao;
            this.loader = loader;
        }

        @Override
        protected Restaurant doInBackground(final Integer... params) {
            return mAsyncTaskDao.getRestaurantFromId(params[0]);
        }

        @Override
        protected void onPostExecute(Restaurant restaurant) {
            loader.onRestaurantLoaded(restaurant);
        }
    }

    private static class loadAllRestaurantsAsyncTask extends AsyncTask<Void, Void, List<Restaurant>> {

        private RestaurantDao mAsyncTaskDao;
        private ListRestaurantLoader loader;

        loadAllRestaurantsAsyncTask(RestaurantDao dao, ListRestaurantLoader loader) {
            mAsyncTaskDao = dao;
            this.loader = loader;
        }

        @Override
        protected List<Restaurant> doInBackground(final Void... params) {
            return mAsyncTaskDao.getListRestaurant();
        }

        @Override
        protected void onPostExecute(List<Restaurant> restaurants) {
            loader.onListRestaurantsLoaded(restaurants);
        }
    }
}
