package esercitazioni.core;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

public class RestaurantViewModel extends AndroidViewModel {

    private RestaurantRepository restaurantRepository;

    public RestaurantViewModel(@NonNull Application application) {
        super(application);
        restaurantRepository = new RestaurantRepository(application);
    }

    public void setLoader(RestaurantRepository.ListRestaurantLoader listRestaurantLoader) {
        restaurantRepository.setLoaders(listRestaurantLoader, null);
    }

    public void setLoader(RestaurantRepository.ActualRestaurantLoader actualRestaurantLoader) {
        restaurantRepository.setLoaders(null, actualRestaurantLoader);
    }

    public void loadListRestaurants() {
        restaurantRepository.loadListRestaurant();
    }

    public void loadRestaurant(int id) {
        restaurantRepository.loadRestaurant(id);
    }

    public void insertRestaurant (Restaurant restaurant) {
        restaurantRepository.insertRestaurant(restaurant);
    }

    public void deleteRestaurant (Restaurant restaurant) {
        restaurantRepository.deleteRestaurant(restaurant);
    }

    public void updateRestaurant (Restaurant restaurant) {
        restaurantRepository.updateRestaurant(restaurant);
    }
}
