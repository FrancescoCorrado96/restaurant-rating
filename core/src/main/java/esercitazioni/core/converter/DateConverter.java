package esercitazioni.core.converter;

import android.arch.persistence.room.TypeConverter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateConverter {
    @TypeConverter
    public static Date toDate(String timestamp) {
        if (timestamp.isEmpty())
            return null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);
        try {
            return sdf.parse(timestamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @TypeConverter
    public static String dateToString(Date date) {
        if (date != null) {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);
            return df.format(date);
        }
        return "";
    }
}