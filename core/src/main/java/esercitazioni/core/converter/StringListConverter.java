package esercitazioni.core.converter;


import android.arch.persistence.room.TypeConverter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StringListConverter {
    private static final String DIVIDER = "._._.";

    @TypeConverter
    public static List<String> stringToStringList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(data.split(DIVIDER));
    }

    @TypeConverter
    public static String stringListToString(List<String> someData) {
        String result = "";
        for(String s : someData) {
            result = result.concat(s).concat(DIVIDER);
        }
        if(!result.isEmpty()) {
            return result.substring(0, result.length() - DIVIDER.length());
        }
        return result;
    }
}
