package esercitazioni.core;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Restaurant.class}, version = 1, exportSchema = false)
public abstract class RestaurantDatabase extends RoomDatabase {

    public static String DATABASE_NAME = "RestaurantDb";

    public abstract RestaurantDao restaurantDao();

    private static RestaurantDatabase INSTANCE;


    static RestaurantDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RestaurantDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RestaurantDatabase.class, DATABASE_NAME)
                            .build();

                }
            }
        }
        return INSTANCE;
    }

}