package esercitazioni.core;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface RestaurantDao {


    @Query("SELECT * FROM ristoranti ORDER BY nome DESC")
    List<Restaurant> getListRestaurant();

    @Query("SELECT * FROM ristoranti WHERE id = :id")
    Restaurant getRestaurantFromId(int id);

    @Insert
    void insertRestaurant(Restaurant restaurant);

    @Delete
    void deleteRestaurant(Restaurant restaurant);

    @Update
    void updateRestaurant(Restaurant restaurant);

}
