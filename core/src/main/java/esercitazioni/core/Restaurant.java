package esercitazioni.core;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import esercitazioni.core.converter.DateConverter;
import esercitazioni.core.converter.StringListConverter;
import io.reactivex.annotations.NonNull;

@Entity(tableName = "ristoranti")
public class Restaurant implements Parcelable {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "nome")
    private String name;

    @ColumnInfo(name = "indirizzo")
    private String address;

    @ColumnInfo(name = "immagini")
    @TypeConverters(StringListConverter.class)
    private List<String> images;

    @ColumnInfo(name = "rating")
    private float rating;

    @ColumnInfo(name = "data")
    @TypeConverters(DateConverter.class)
    private Date date;

    public Restaurant(@NonNull String name,@NonNull String address, float rating,@NonNull Date date, List<String> images) {
        this.name = name;
        this.address = address;
        this.rating = rating;
        this.date = date;
        this.images = images;
    }

    @Ignore
    public Restaurant(@NonNull int id, @NonNull String name,@NonNull String address, float rating,@NonNull Date date, List<String> images) {
        this(name, address, rating, date, images);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImage(int pos) {
        return images.get(pos);
    }

    public void setImage(String image) {
        this.images.add(image);
    }

    public void setListImages (List<String> images) {
        this.images = new ArrayList<>(images);
    }

    public List<String> getImages() {
        return images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.address);
        dest.writeStringList(this.images);
        dest.writeFloat(this.rating);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
    }

    protected Restaurant(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.address = in.readString();
        this.images = in.createStringArrayList();
        this.rating = in.readFloat();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
    }

    public static final Parcelable.Creator<Restaurant> CREATOR = new Parcelable.Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel source) {
            return new Restaurant(source);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };
}
